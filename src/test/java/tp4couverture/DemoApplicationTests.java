package tp4couverture;

//import static org.junit.jupiter.api.Assertions.assertAll;
//import static org.junit.jupiter.api.Assertions.assertArrayEquals;
//import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

//import java.util.ArrayList;
import java.util.HashMap;

//import org.assertj.core.api.Assert;

//import org.junit.jupiter.api.AssertTrue;

//import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;

//@SpringBootTest
class DemoApplicationTests {
	Lettre l1 = new Lettre();
	ObjetPostal l2 = new Lettre("Sommières", "Montpellier", "34000", 0.06, 
	0.005, Tauxrecom.faible, false);
	ObjetPostal l3 = new Colis("Nîmes","Montpellier","34000",1,0.4,
	Tauxrecom.fort,"chocolat",50);
	ObjetPostal l4 = new ColisExpress("Montpellier","Sommières","30000",1,0.4,
				Tauxrecom.fort,"chocolat",50,true);	
	ObjetPostal l5 = new ColisExpress("Montpellier","Sommières","30000",1,0.4,
				Tauxrecom.fort,"gâteaux secs",70,true);	

	@Test
	void testvolumelettre1(){
		assertTrue(l1.getVolume()==0);
	}

	@Test
	void testvolumenonvidelettre1(){
		assertEquals(l1.toString(),"Lettre code postal inconnu/destination inconnue/faible/ordinaire");
	}
	
	@Test
	void testvolumelettre2(){
		assertEquals(0.005, l2.getVolume());

		//assertTrue(l2.getVolume()==0.005);
	}

	@Test
	void testoriginelettre2(){
		assertEquals(l2.getOrigine(),"Sommières");
	}

	
	@Test
	void testtostringlettre2(){
		assertEquals(l2.toString(), ("Lettre 34000/Montpellier/faible/ordinaire"));
	}


	@Test
	void testdestinationlettre2(){
		assertEquals(l2.getDestination(), "Montpellier");
	}

	@Test
	void testcodepostallettre2(){
		assertEquals(l2.getCodePostal(),"34000");
	}

	@Test
	void testgetpoidslettre2(){
		assertTrue(l2.getPoids()==0.06);
	}

	
    @Test
	void testpoidscolis1(){
		assertTrue(l3.getPoids()==1);
	}

	@Test
	void testoriginecolis1(){
		assertTrue(l3.getOrigine()=="Nîmes");
	}

	@Test
	void testdestinationcolis1(){
		assertEquals(l3.toString(), ("Colis 34000/Montpellier/fort/0.4/50.0"));
	}

	@Test
	void testcodepostalcolis1(){
		assertEquals(l3.getCodePostal(), ("34000"));
	}

	@Test
	void testvolumecolis1(){
		assertTrue(l3.getVolume()== 0.4);
	}


	@Test
	void testvolumecolisexpress1(){
		assertTrue(l4.getVolume()==0.4);
	}


	@Test
	void testoriginecolisexpress1(){
		assertEquals(l4.getOrigine(),"Montpellier");
	}



	@Test
	void testdestinationcolisexpress1(){
		assertEquals(l4.getDestination(), ("Sommières"));
}

    @Test
	void testcodepostalcolisexpress1(){
		assertEquals(l4.getCodePostal(), "30000");
}

    @Test
	void testpoidscolisexpress1(){
		assertTrue(l4.getPoids()== 1.0);
}


    @Test
	void testpoidscolisexpress2(){
		assertTrue(l5.getPoids()==1);
	}

	@Test
	void testvolumecolisexpress2(){
		assertTrue(l5.getVolume()==0.4);
	}


	@Test
	void testtostringcolisexpress2(){
		assertEquals(l5.toString(), ("ColisExpress 30000/Sommières/fort/0.4/70.0/1.0/14 /70.070.0"));
		}

	@Test
	void testcodepostalcolisexpress2(){
		assertEquals(l5.getCodePostal(),"30000");
		}

	@Test
	void testoriginecolisexpress2(){
		assertEquals(l5.getOrigine(),"Montpellier");
		}

	@Test
	void testdestinationcolisexpress2(){
		assertEquals(l5.getOrigine(),"Montpellier");
		}


		//test pour sacpostal
		SacPostal sac1 = new SacPostal();

	@Test
	void testcapacitesacpostal1(){
		assertTrue(sac1.getCapacite()==0.5);
		}


	@Test
	void testvolumesacpostal1(){
		assertTrue(sac1.volume()==0.005);
		}


	@Test
	void testvolumeinterieursacpostal1(){
		assertTrue(sac1.volumeInterieur()==0);
		}


	@Test
	void testvaleurremboursementsacpostal1(){
			assertTrue(sac1.valeurRemboursement()==0);
		}

	@Test
	void testvolumeobjetpostalsacpostal1(){
			assertTrue(sac1.volumeObjetsCodePostal("34000")==0.0);
		}

	@Test
	void testextrairecodesacpostal1(){
			assert(sac1.extraitCodePostal("34000")!=null);
		}

		@Test
	void testtostringsacpostal1(){
			assertEquals(sac1.toString(),("SacPostal [listeOP=0 [], capacite=0.5]"));
		}



		SacPostal sac2 = new SacPostal(574);


	@Test
	void testcapacitesacpostal2(){
		assertTrue(sac2.getCapacite()==574);
			}





	NoticeBibliographique notice1 =new NoticeBibliographique("isbn","titre", "soustitre");
	private Object contenu;
	

	@Test
	void testisbnnotice1(){
		assertEquals(notice1.getIsbn(),"isbn");
		}


	@Test
	void testtitrenotice1(){
		assertEquals(notice1.getTitre(),"titre");
		}
	

	@Test
	void testsoustitrenotice1(){
		assertEquals(notice1.getSousTitre(),"soustitre");
		}
		



		//test pour catalogue
		

	@Test
	void testcontenucatalogue1(){
		Catalogue catalogue1=new Catalogue();

		assertEquals(catalogue1.toString(),"");
			}

	

	//@Test
	//void testajoutunenotice(){
	//	Catalogue catalogue1=new Catalogue();
	//	NoticeBibliographique notice1=new NoticeBibliographique("3");
	//	catalogue1.ajoute(notice1);
	//	assert(((HashMap<String, NoticeBibliographique>) this.contenu).containsKey(notice1.getIsbn()));		 
	
	//}


	//@Test
	//void testrecherchepartitrecatalogue1(){
	//	assert (catalogue1.rechercheParIsbn("titre")) != null;
	
	//}


	//@Test
	//void testrecherchepartitre2catalogue1(){
	//	assert (catalogue1.rechercheParIsbn("titre")) != null;
	
	//}

	//@Test
	//void testrecherchepartitre3catalogue1(){
	//	assert (catalogue1.rechercheParIsbn("titre")) != null;
	
	//}

	//@Test
	//void testrechercheparauteurcatalogue1(){
	//	assert (catalogue1.rechercheParAuteur("")) != null;
	
	//}



	//test pour demoapplication


	@Test
	void testoriginelettre1(){
		assertEquals(l1.getOrigine(),"origine inconnue");
			}
			
		
	@Test
	void testnewlettre1(){
		equals(l1=new Lettre());
			}

	
	@Test
	void testnewlettre2(){
		equals(l2=new Lettre("Sommières", "Montpellier", "34000", 0.06, 
		0.005, Tauxrecom.faible, false));
			}
			
		 
			
	@Test
	void testajouteauxsac2(){
		SacPostal sac3 = new SacPostal();
		sac3.ajoute(l4);
		assertTrue(sac3.getCapacite()==0.5);
		
	}



	}
	