package tp4couverture;

//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		//System.out.println("hello");
		//SpringApplication.run(DemoApplication.class, args);
		Lettre l1 = new Lettre();
		
		ObjetPostal l2 = new Lettre("Sommières", "Montpellier", "34000", 0.06, 
				0.005, Tauxrecom.faible, false);
		
		l1.affiche();
		l2.affiche();
		
		ObjetPostal l3 = new Colis("Nîmes","Montpellier","34000",1,0.4,
				Tauxrecom.fort,"chocolat",50);
		l3.affiche(); // pendant affiche, this=l3
		// cela appelle affiche de ObjetPostal
		// qui contient this.toString() avec this=l3
		// donc on cherche la méthode toString à partir
		// de la classe (new) de l3, càd Colis
		
		/*
		 * String origine, String destination, String codePostal, double poids, double volume,
			Tauxrecom tauxRec, String declContenu, double valeurDecl,
			boolean emballage
		 */
		
		ObjetPostal l4 = new ColisExpress("Montpellier","Sommières","30000",1,0.4,
				Tauxrecom.fort,"chocolat",50,true);	
		ObjetPostal l5 = new ColisExpress("Montpellier","Sommières","30000",1,0.4,
				Tauxrecom.fort,"gâteaux secs",70,true);	
		l4.affiche();
		l5.affiche();	
		
		SacPostal sac1 = new SacPostal();
		System.out.println(sac1.toString());
		sac1.ajoute(l5);
		sac1.ajoute(l4);
		sac1.ajoute(l3);
		sac1.ajoute(l2);
		sac1.ajoute(l1);
		
		System.out.println(sac1);
		
		System.out.println("volume intérieur "+sac1.volumeInterieur());
		System.out.println("tarif remboursement "+sac1.valeurRemboursement());
		
		/*SacPostal sac3 = sac1.transfereSac("30000");
		System.out.println("sac 3 :"+sac3);
	
		SacPostal sac2 = sac1.extraitCodePostal("30000");
		System.out.println("sac 2 :"+sac2);
		*/
		
		SacPostal sac4 = new SacPostal(0.00001);
		sac1.extraitCodePostal("30000",sac4);
		System.out.println("sac 4 :"+sac4);		
		
	}

}
