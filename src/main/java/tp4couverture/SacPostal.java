//package objetPostal;
package tp4couverture;

import java.util.ArrayList;

public class SacPostal {
	
	// mettre en place l'association unidirectionnelle
	// de SacPostal vers ObjetPostal par un attribut
	// comme la multiplicité est *
	// le type de l'attribut est tableau, séquence ... (plusieurs valeurs)
	// sans se soucier du fait que ObjetPostal est abstraite
	
	private ArrayList<ObjetPostal> contenu = new ArrayList<>();
	
	//on aurait pu envisager :
	// private ArrayList<Colis> listeOPC = new ArrayList<>();	
	// private ArrayList<Lettre> listeOPL = new ArrayList<>();
	// non extensible dans l'idée de l'objet
	// dès qu'on a une nouvelle sous-classe de ObjetPostal il faudrait faire
	// une nouvelle liste
	
	// on peut faire cette version VFinal où une fois choisie
	// la capacité du sac n'évoluera plus -> final
	// private final double capacite;
	
	private double capacite; // ici on suppose qu'elle pourra évoluer


	public SacPostal() {
		this.capacite = 0.5; // m3
	}
	
	public SacPostal(double cap) {
		this.capacite = cap;
	}

	public double getCapacite() {
		return capacite;
	}
	
	// pas de setCapacite dans VFinal car 'final'
	
	// pour la version où capacité peut évoluer
	
	public void setCapacite(double nouvCapacite) {
		if (nouvCapacite >0) {
			this.capacite = nouvCapacite;
			System.out.println("augmentation de capacité");
		}
		else System.out.println("pas de capacité négative ou nulle");
	}

	// éviter : cela donne accès à liste et
	// n'importe quelle autre classe pourra faire
	// n'importe quoi avec
	/*public SacPostal(double cap, ArrayList<ObjetPostal> listeOP) {
		this.capacite = cap;
		this.contenu = listeOP;
		}
	 */
	
	public boolean ajoute(ObjetPostal o) {
				// on n'ajoute pas s'il est déjà dedans
				if (this.contenu.contains(o)) {
					System.out.println("objet déjà dans le sac");
					return false;
				}
				// on ajoute l'objet s'il rentre dans le sac
				// volume des objets dans le sac + volume de o
				// doit être inférieur à la capacité
				if (this.volumeInterieur()+o.getVolume() <= this.capacite)
					{this.contenu.add(o);return true;}
				else 
					{System.out.println("l'objet "+ o+ " ne rentre "
							+ "pas (en volume) dans le sac");
					return false;
					}
	}
	
	public double volumeInterieur() { //m3
		double result = 0;
		for (ObjetPostal o : this.contenu)
			result = result + o.getVolume();
		return result;
	}
	
	// On veut connaitre le volume occupe par un
	// sac (compter forfaitairement 5dm3 = 0.005m3 pour la toile du sac)
	
	public double volume() {
		return this.volumeInterieur()+0.005;
	}
	
	public double valeurRemboursement() {
		double result = 0;
		for (int i = 0; i < this.contenu.size(); i++)
			result += this.contenu.get(i).tarifRemb();
		// l'appel de tarifRemb() fonctionne
		// grâce à la liaison dynamique
		// suivant la classe (new) de l'objet this.contenu.get(i)
		// on va choisir dans cette classe la 'bonne' 
		// méthode : la plus spécifique
		// on part de la classe (new) de l'objet et on cherche
		// la méthode tarifRemb dans cette classe et en remontant si besoin
		// message en cascade typique de l'objet
		// message = appel méthode ou accès attribut
		return result;		
	}
	
	public double volumeObjetsCodePostal(String codePostal) {
		double result = 0;
		for (ObjetPostal o : this.contenu)
			if (o.getCodePostal().equals(codePostal))
				result += o.getVolume();
		return result;
	}
	
	public SacPostal extraitCodePostal(String codePostal) {
		// penser à créer un sac assez grand
		// méthode paresseuse
		// SacPostal result = new SacPostal(this.volumeInterieur());
		// méthode pas paresseuse :
		// calculer en premier le volume des objets 
		// qui ont le code postal qui nous intéresse
		// méthode auxiliaire
		
		SacPostal result = new SacPostal(
				this.volumeObjetsCodePostal(codePostal));
		
		for (ObjetPostal o : this.contenu) {
			if (o.getCodePostal().equals(codePostal))
				result.ajoute(o); // pas add ! result est un sac
								  // pas une liste
		}
		
		this.contenu.removeAll(result.contenu);
		return result;
	}
	
	public SacPostal transfereSac(String cp) {
        SacPostal nvSac = new SacPostal(0.00001); // attention au volume
        for (ObjetPostal obj : this.contenu) {
            if (obj.getCodePostal().equals(cp)) {
                if (!nvSac.ajoute(obj)) {
                    nvSac.setCapacite(nvSac.capacite + obj.getVolume());
                    nvSac.ajoute(obj);
                }
                //this.contenu.remove(obj);
                // périlleux avec les vieux compilateurs et interprètes
            }
        }
        this.contenu.removeAll(nvSac.contenu);
        return nvSac;
    }
	
	public void extraitCodePostal(String codePostal, SacPostal autreSac) 
	{
		if (autreSac.capacite <
				autreSac.volumeInterieur()+
					this.volumeObjetsCodePostal(codePostal))
		{
			autreSac.setCapacite(autreSac.volumeInterieur()+
					this.volumeObjetsCodePostal(codePostal));
		}
		// ici je peux faire l'ajout, 
		// on est sûrs d'avoir la place
		for (ObjetPostal o : this.contenu) {
			if (o.getCodePostal().equals(codePostal))
				autreSac.ajoute(o);
		}
	}
	
	@Override
	public String toString() {
		return "SacPostal [listeOP=" + contenu.size() + " "+
				contenu + 
				", capacite=" + capacite + "]";
	}
}
