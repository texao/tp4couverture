//package bibliotheque;
package tp4couverture;

//import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Catalogue {
	
	/*
	 *  mise en place de l'association qualifiée
	 *  catalogue+ISBN -> 1 notice bibliographique
	 *  l'attribut choisi est une map
	 *  une map d'un point de vue 'théorique'
	 *  associe à l'isbn la notice de manière rapide
	 *  clef d'accès : isbn (String)
	 *  valeur associée : notice (NoticeBibliographique)
	 */
	
	private Map<String, NoticeBibliographique> contenu;
	

	public Catalogue() {
		this.contenu = new HashMap<String, NoticeBibliographique>();
	}

	/*
	 * méthodes utiles pour le catalogue
	 * 
	 * - ajouter une notice
	 * 
	 * - chercher une ou plusieurs notice
	 * -- par son isbn (une car isbn est un identifiant)
	 * -- par son titre (plusieurs car titre n'est pas un identifiant)
	 * -- par un de ses auteurs (plusieurs car auteur n'est pas un identifiant)
	 * 
	 * - toString montrant les notices présentes
	 * dans le catalogue
	 * 
	 * - dans un main : appeler ces opérations
	 */
	
	public void ajoute(NoticeBibliographique notice) {
		if (contenu.containsKey(notice.getIsbn()))
		{
		System.out.println("Le catalogue contient déjà "
					+ "une notice avec cet ISBN! "
					+ "Elle va être remplacée par la nouvelle \n");
		}
		contenu.put(notice.getIsbn(),notice);
	}
	
	public NoticeBibliographique rechercheParIsbn(String ISBN) {
		  if(contenu.containsKey(ISBN)) 
			  return contenu.get(ISBN);
		   else{
		     System.out.println("Le catalogue ne contient pas "
		     					+ "de notice avec cet ISBN! \n");
		     return null;
		   }
	}
	
	public ArrayList<NoticeBibliographique> rechercheParTitre(String titre) {
		ArrayList<NoticeBibliographique> liste = new ArrayList<>();
		// on doit parcourir la hashmap
		for (NoticeBibliographique notice : this.contenu.values())
			if (notice.getTitre().equals(titre))
				liste.add(notice);		
		return liste;
	}
	
	public ArrayList<NoticeBibliographique> rechercheParTitre2(String titre) {
		ArrayList<NoticeBibliographique> liste = new ArrayList<>();
		// on doit parcourir la hashmap
		for (String isbn : this.contenu.keySet()) {
			NoticeBibliographique notice = contenu.get(isbn);
			if (notice.getTitre().equals(titre))
				liste.add(notice);	
		}
		return liste;
	}
	
	public ArrayList<NoticeBibliographique> rechercheParTitre3(String titre) {
		ArrayList<NoticeBibliographique> liste = new ArrayList<>();
		// on doit parcourir la hashmap
		for (Entry<String, NoticeBibliographique>
								paire : this.contenu.entrySet()) {
			NoticeBibliographique notice = paire.getValue();
			if (notice.getTitre().equals(titre))
				liste.add(notice);	
		}
		return liste;
	}
	
	public ArrayList<NoticeBibliographique>  rechercheParAuteur(String auteur) {
		return null;
	}
	
	public String toString() {
		return "";
	}
}
