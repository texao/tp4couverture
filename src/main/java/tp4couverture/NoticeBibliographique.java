//package bibliotheque;
package tp4couverture;

//import java.util.ArrayList;

public class NoticeBibliographique {
	
	// attributs propres 
	private final String isbn; // identifiant
	private String titre, sousTitre;
	
	// attributs faisant la connexion entre Notice et
	// Contribution
	
	//private ArrayList<Contribution> listeContrib;

	public NoticeBibliographique(String isbn, String titre, String sousTitre) {
		super();
		this.isbn = isbn;	
		this.titre = titre;
		this.sousTitre = sousTitre;
	}

	public NoticeBibliographique(String isbn) {
		this.isbn=isbn;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getSousTitre() {
		return sousTitre;
	}

	public void setSousTitre(String sousTitre) {
		this.sousTitre = sousTitre;
	}

	public String getIsbn() {
		return isbn;
	}

	
}
